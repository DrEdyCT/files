Flask==0.10.1
Flask-Security==1.7.4
Jinja2==2.7.3
SQLAlchemy==0.9.8
Flask-SQLAlchemy==2.0
psycopg2==2.5.4
Flask-Migrate
