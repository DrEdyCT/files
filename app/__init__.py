# -*- coding: utf-8 -*-
from flask import Flask
from flask.ext.migrate import MigrateCommand
from flask.ext.security import SQLAlchemyUserDatastore, Security
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.script import Manager
import os


def create_app(config_object=None):
    template_folder = os.path.abspath(os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        'templates'
    ))
    static_folder = os.path.abspath(os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        os.path.pardir,
        'static'
    ))
    app = Flask(
        'filecesspool',
        template_folder=template_folder,
        static_folder=static_folder
    )
    if config_object is None:
        config_object = 'config'
    app.config.from_object(config_object)

    @app.errorhandler(404)
    def not_found(error):
        return "Not found :(", 404

    return app


def init_security(app, db):
    from app.models import User, Role
    from app.forms import RegisterForm
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, datastore=user_datastore, register_form=RegisterForm)


def init_urls():
    import views

RunCommand = Manager(usage='Perform database migrations')

app = create_app()
db = SQLAlchemy(app)
security = Security()
init_security(app, db)
init_urls()

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('run')
