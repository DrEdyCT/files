# -*- coding: utf-8 -*-
import os

_basedir = os.path.abspath(os.path.dirname(__file__))


def get_postgres_uri(login, password, host, port, db=None):
    if db is None:
        db = ''
    return 'postgresql+psycopg2://{login}:{password}@{host}:{port}/{db}'.format(
        login=login,
        password=password,
        host=host,
        port=port,
        db=db
    )

SECRET_KEY = 'This string will be replaced with a proper key in production.'

SQLALCHEMY_LOGIN = os.environ.get(
    'OBLICHI_CONTENT_POSTGRES_LOGIN',
    'postgres'
)
SQLALCHEMY_PASSWORD = os.environ.get(
    'OBLICHI_CONTENT_POSTGRES_PASSWORD',
    'postgres'
)
SQLALCHEMY_HOST = os.environ.get(
    'OBLICHI_CONTENT_POSTGRES_HOST',
    'localhost'
)
SQLALCHEMY_PORT = os.environ.get(
    'OBLICHI_CONTENT_POSTGRES_PORT',
    5432
)
SQLALCHEMY_DB = os.environ.get(
    'OBLICHI_CONTENT_DB',
    'filecesspool'
)

SQLALCHEMY_DATABASE_URI = get_postgres_uri(
    SQLALCHEMY_LOGIN,
    SQLALCHEMY_PASSWORD,
    SQLALCHEMY_HOST,
    SQLALCHEMY_PORT,
    SQLALCHEMY_DB
)

SECURITY_REGISTERABLE = True
SECURITY_SEND_REGISTER_EMAIL = False
SECURITY_PASSWORD_HASH = 'sha256_crypt'
SECURITY_PASSWORD_SALT = 'qwerty'

FILES_LIMIT = 100

UPLOAD_FOLDER = _basedir + '/media/'
